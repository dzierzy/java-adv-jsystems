<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>Order</h1>
                <table border="1">
                    <tr>
                        <th>Type</th>
                        <th>Price</th>
                    </tr>
                    <xsl:for-each select="//product">
                        <tr>
                            <td><xsl:value-of select="@class"/></td>
                            <td><xsl:value-of select="price"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
