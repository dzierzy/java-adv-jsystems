package pl.jsystems.functional;

import pl.jsystems.product.Book;
import pl.jsystems.product.Product;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionalStarter {

    public static void main(String[] args) {


        Comparator<Integer> intComparator = Comparator.comparingInt(i -> i);

        List<Book> books = new ArrayList<>();
        books.add(new Book("Thinking in Java", 5));
        books.add(new Book("Essential Java", 15));
        books.add(new Book("Essential Java", 15));
        books.add(new Book("Functional Programming", 12));
        books.add(new Book("Reactive Programming", 5));


        Map<Integer,List<Book>> groupedBooks =
        books.stream().collect(Collectors.groupingBy(b->b.getPrice()));

        System.out.println(groupedBooks);

        long start = System.currentTimeMillis();
        //books =
        List<String> titles =
                books.stream()
                .sorted(Comparator.comparingInt(Product::getPrice))
                .filter(b->b.getPrice()<=12)
                .skip(1)
                .limit(1)
                .map(b->b.getTitle())
                .collect(Collectors.toList());
        long end = System.currentTimeMillis();
        System.out.println("millis=" + (end-start));

        Stream<Book> bookStream = books.stream();

        if(!bookStream.isParallel()){
            bookStream = bookStream.parallel();
        }

        if(bookStream.isParallel()){
            bookStream = bookStream.sequential();
        }

        //long count =
        //boolean match =
        Optional<Book> optionalBook =
        books.stream()
                .distinct()
                .limit(3)
                .peek(b-> System.out.println(b))
                //.count();
                //.allMatch(b->b.getTitle().contains("Java"));
                //.filter(b->b.getTitle().contains("xyz"))
                .findFirst();

        Book b = optionalBook.orElseThrow(()->new IllegalArgumentException("empty optional"));
                //.orElseGet(()->new Book());
                //.orElse(new Book("Instead of null", 0));
        System.out.println(b);

        // or
        optionalBook.ifPresent(System.out::println);


        System.out.println(titles);

        Stream<String> stringStream = Stream.iterate("5", s->s+"0").limit(10);
                //.generate(()->"4").limit(10);
                //.of("1", "2", "3");
        stringStream.forEach(s-> System.out.println(s));
    }
}
