package pl.jsystems.functional;

@FunctionalInterface
public interface Checker {

    boolean check(int value);

    default String checkDescription(){
        return "Undefined";
    }

    //void foo();

    static void doNothing(){
        System.out.println("doing nothing");
    }
}
