package pl.jsystems.generics;

import java.util.ArrayList;
import java.util.List;
// T - type
// E - enumaration
// V - value
// K - key
public class Cart<E> {

    protected List<E> list = new ArrayList<>();

    public void addProduct(E t){
        list.add(t);
    }

    public void removeProduct(E t){
        list.remove(t);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "list=" + list +
                '}';
    }
}
