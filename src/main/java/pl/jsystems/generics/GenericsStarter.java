package pl.jsystems.generics;

import pl.jsystems.product.Book;
import pl.jsystems.product.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class GenericsStarter {

    // psvm
    public static void main(String[] args){
        Box<StringBuilder> box = new Box<>();
        box.setO(new StringBuilder("Sample Text"));

        StringBuilder o = box.getO();
        System.out.println(o.toString().toUpperCase()); // sout


        Cart<Product> productCart = new ProductCart();

        //productCart.addProduct(new Product());
        productCart.addProduct(new Book("Hamlet", 999));
        productCart.addProduct(new Book("Pan Tadeusz", 9999));

        if(productCart instanceof ProductCart) {
            System.out.println(((ProductCart) productCart).sumPrice());
        }

        Book[] productArray = new Book[3];
        productArray[0] = new Book("Wiedzmin", 666);
        productArray[1] = new Book("Starnger Things", 555);
        productArray[2] = new Book("Game Of Thrones", 444);

        List<Book> products = fromArray(productArray);

        addGratis(products);
        int sum = GenericsStarter.sumPrice(products);
        System.out.println("price=" + sum);

    }

    public static <T> List<T> fromArray(T[] o){
        return new ArrayList<>(Arrays.asList(o)); // var-args
    }

    // PECS - Producer Extends, Consumer Super

    public static int sumPrice(List<? extends Product> products){
        int price = 0;
        for(Product p : products){
            price += p.getPrice();
        }
        return price;
    }

    public static void addGratis(List<? super Book> products){
        products.add(new Book("Harlequin", 0));
    }

}
