package pl.jsystems.generics;

import pl.jsystems.product.Product;

public class ProductCart extends Cart<Product> {

    public int sumPrice(){
        int price = 0;
        for(Product p : this.list){
            price += p.getPrice();
        }
        return price;
    }
}
