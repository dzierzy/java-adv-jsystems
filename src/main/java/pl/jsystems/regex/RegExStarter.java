package pl.jsystems.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExStarter {

    public static void main(String[] args) {


        String text = "12345 12345 ul. grojecka 1/3, 00-950 warszawa";
        String expression = "\\b\\d{2}-?\\d{3}\\b";

        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            System.out.println(matcher.group());
            System.out.println(matcher.start() + "_" + matcher.end());
        }



    }
}
