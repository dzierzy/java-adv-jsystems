package pl.jsystems.xml;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XsltStarter {

    public static void main(String[] args) throws TransformerException {

        TransformerFactory tf = TransformerFactory.newInstance();

        StreamSource xml = new StreamSource(XsltStarter.class.getResourceAsStream("/order.xml"));
        StreamSource xslt = new StreamSource(XsltStarter.class.getResourceAsStream("/order.xslt"));
        StreamResult html = new StreamResult("d:\\order.html");

        Transformer t = tf.newTransformer(xslt);
        t.transform(xml, html);

    }
}
