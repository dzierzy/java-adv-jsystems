package pl.jsystems.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class ProductHandler extends DefaultHandler {

    private boolean products;

    private ProductDescription pd;

    private List<ProductDescription> descriptions = new ArrayList<>();

    private String propertyName;


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("products")){
            products = true;
        } else if(qName.equals("product") && products){
            pd = new ProductDescription();
            pd.setProductClass(attributes.getValue("class"));
        } else if(pd!=null){
            propertyName = qName;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equals("products")){
            products = false;
        } else if(qName.equals("product") && products){
            descriptions.add(pd);
            pd = null;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        if(propertyName!=null && products){
            pd.getProperties().put(propertyName, new String(ch, start, length));
            propertyName = null;
        }
    }

    public List<ProductDescription> getDescriptions() {
        return descriptions;
    }
}
