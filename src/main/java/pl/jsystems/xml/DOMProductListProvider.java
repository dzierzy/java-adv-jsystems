package pl.jsystems.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DOMProductListProvider implements ProductListProvider {

    private Document document;

    public DOMProductListProvider(String xmlFile){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(DOMProductListProvider.class.getResourceAsStream(xmlFile));
            Element e = document.getDocumentElement();
            String orderDate = e.getAttribute("date");
            String tagName = e.getTagName();
            System.out.println("docuemnt loaded. order date=" + orderDate +", root tag: " + tagName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ProductDescription> getProductDescriptions() {

        List<ProductDescription> descriptions = new ArrayList<>();

        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();

        NodeList nodes = null;
        try {
            XPathExpression xpathExpression = xpath.compile("//products/product");
            nodes = (NodeList)xpathExpression.evaluate(document, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
           throw new RuntimeException(e);
        }


        //NodeList nodes = document.getDocumentElement().getElementsByTagName("product");

        for(int i=0; i<nodes.getLength(); i++){
            Node n = nodes.item(i);
            if(n.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) n;
                String productClass = e.getAttribute("class");
                NodeList propNodes = e.getChildNodes();
                Map<String, String> props = new HashMap<>();
                for(int j=0; j<propNodes.getLength(); j++){
                    Node propNode = propNodes.item(j);
                    if(propNode.getNodeType() == Node.ELEMENT_NODE){
                        Element propElement = (Element) propNode;
                        String propertyName = propElement.getTagName();
                        String propertyValue = propElement.getTextContent();
                        props.put(propertyName, propertyValue);
                    }
                }
                ProductDescription pd = new ProductDescription();
                pd.setProductClass(productClass);
                pd.setProperties(props);
                descriptions.add(pd);
            }
        }

        return descriptions;
    }
}
