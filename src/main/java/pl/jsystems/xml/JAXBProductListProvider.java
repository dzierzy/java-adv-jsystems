package pl.jsystems.xml;

import pl.jsystems.product.Product;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.util.List;

public class JAXBProductListProvider implements ProductListProvider {

    private String xmlFile;

    public JAXBProductListProvider(String xmlFile) {
        this.xmlFile = xmlFile;
    }

    @Override
    public List<ProductDescription> getProductDescriptions() {

        try {
            JAXBContext context = JAXBContext.newInstance(Products.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Products products = (Products) unmarshaller.unmarshal(JAXBProductListProvider.class.getResourceAsStream(xmlFile));

            return products.getDescriptions();

        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
