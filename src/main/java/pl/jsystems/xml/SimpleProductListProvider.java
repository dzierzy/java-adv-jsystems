package pl.jsystems.xml;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleProductListProvider implements ProductListProvider{
    @Override
    public List<ProductDescription> getProductDescriptions() {
        ProductDescription pd = new ProductDescription();
        pd.setProductClass("pl.jsystems.product.Newspaper");

        Map<String, String> props = new HashMap<>();
        props.put("price", "99");
        props.put("title", "Bajtek");
        props.put("publisher", "Laboratorium Komputerowe Avalon");
        props.put("releaseDate", "01-01-1989");
        pd.setProperties(props);

        return Arrays.asList(pd);
    }
}
