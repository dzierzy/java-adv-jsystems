package pl.jsystems.xml;

import java.util.List;

public interface ProductListProvider {

    List<ProductDescription> getProductDescriptions();

}
