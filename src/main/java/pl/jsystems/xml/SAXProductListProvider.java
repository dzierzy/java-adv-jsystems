package pl.jsystems.xml;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SAXProductListProvider implements ProductListProvider {

    private String xmlFile;

    SAXParser parser;

    public SAXProductListProvider(String xmlFile){
        this.xmlFile = xmlFile;
        SAXParserFactory f = SAXParserFactory.newInstance();
        try {
            parser = f.newSAXParser();
        } catch (ParserConfigurationException | SAXException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<ProductDescription> getProductDescriptions() {

        ProductHandler handler = new ProductHandler();
        try {
            parser.parse(
                    SAXProductListProvider.class.getResourceAsStream(xmlFile),
                    handler);
        } catch (SAXException | IOException e) {
            throw new RuntimeException(e);
        }

        return handler.getDescriptions();
    }
}
