package pl.jsystems.xml;

import pl.jsystems.product.Product;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class XmlStarter {

    static Map<Class, Function<String, Object>> mappers = new HashMap<>();

    static {
        mappers.put(int.class, s->Integer.parseInt(s));
        mappers.put(double.class, s->Double.parseDouble(s));
        mappers.put(LocalDate.class,
                s->LocalDate.parse(s, DateTimeFormatter.ofPattern("dd-MM-yyyy")));

    }

    public static void main(String[] args) {

        ProductListProvider provider = new JSONProductListProvider("/order.json");
                //new JAXBProductListProvider("/order-jaxb.xml");
                //new SAXProductListProvider("/order.xml");

        List<Product> products = new ArrayList<>();

        provider.getProductDescriptions()
                .forEach(pd->{
                    Product p = getProductFunction().apply(pd);
                    products.add(p);
                });

        System.out.println(products);
    }

    private static Function<ProductDescription, Product> getProductFunction(){
        return pd -> {
            try {
                Class clazz = Class.forName(pd.getProductClass());
                Object o = clazz.newInstance();
                if(!(o instanceof Product)){
                    throw new IllegalArgumentException("the class not instance of Product");
                }
                Product p = (Product)o;

                for(Map.Entry<String,String> entry : pd.getProperties().entrySet()){
                    Field f = getField(clazz, entry.getKey());
                    if(!f.isAccessible()){
                        f.setAccessible(true);
                    }

                    Function<String, Object> mapper = mappers.get(f.getType());
                    if(mapper!=null){
                        f.set(p, mapper.apply(entry.getValue()));
                    } else {
                        f.set(p, entry.getValue());
                    }
                }
                return p;

            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
                throw new RuntimeException("problem while prduct transformation", e);
            }
        };
    }

    public static Field getField(Class clazz, String fieldName) {
        do {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }while(clazz!=null);
        return null;
    }

}
