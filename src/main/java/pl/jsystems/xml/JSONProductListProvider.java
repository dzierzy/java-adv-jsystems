package pl.jsystems.xml;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JSONProductListProvider implements ProductListProvider {

    private String jsonFile;

    public JSONProductListProvider(String jsonFile) {
        this.jsonFile = jsonFile;
    }

    @Override
    public List<ProductDescription> getProductDescriptions() {

        ObjectMapper mapper = new ObjectMapper();
        try {
            Products products = mapper.readValue(
                    JSONProductListProvider.class.getResourceAsStream(jsonFile),
                    Products.class
                    );
            mapper.writeValue(new File("d:\\" + jsonFile+".bis"), products);

            return products.getDescriptions();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
