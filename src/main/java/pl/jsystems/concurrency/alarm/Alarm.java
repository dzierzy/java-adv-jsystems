package pl.jsystems.concurrency.alarm;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class Alarm implements Runnable{


    static ScheduledExecutorService es = Executors.newScheduledThreadPool(5);
            //.newFixedThreadPool(1);

    public void alarm(){

        es.schedule(this, 15, TimeUnit.SECONDS);
                //.execute(this);
    }

    @Override
    public void run() {
        specificAlarm();
    }

    protected abstract void specificAlarm();

}
