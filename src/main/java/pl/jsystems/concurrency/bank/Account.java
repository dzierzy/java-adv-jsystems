package pl.jsystems.concurrency.bank;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger amount;

    public Account(int amount) {
        this.amount = new AtomicInteger(amount);
    }

    public  void deposit(int value){
        //amount+=value;
        /*synchronized (this) {
            amount = amount + value;
        }*/
        amount.addAndGet(value);
    }

    public  void withdraw(int value){
        //if(amount>=value){
        /*synchronized(this) {
            amount = amount - value;
        }*/
        amount.addAndGet(-value);
        //}
    }

    public int getAmount() {
        return amount.get();
    }
}
