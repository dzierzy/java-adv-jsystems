package pl.jsystems.concurrency.bank;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AccountStarter {

    public static void main(String[] args) {

        ExecutorService es = Executors.newFixedThreadPool(3);

        Account a = new Account(1000);

        es.execute(new DepositTask(a));
        es.execute(new WithdrawTask(a));

        es.shutdown();
    }
}
