package pl.jsystems.concurrency.bank;

public class WithdrawTask implements Runnable {


    private Account account;

    public WithdrawTask(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for(int i=0; i<1_000_000; i++){
            account.withdraw(1);
        }
        System.out.println(account.getAmount());
    }
}
