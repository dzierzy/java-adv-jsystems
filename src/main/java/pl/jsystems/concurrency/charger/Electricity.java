package pl.jsystems.concurrency.charger;

public class Electricity {

    private static Electricity instance;

    private volatile boolean on = false;

    private Electricity(){}

    public static Electricity getInstance(){
        if(instance==null){
            instance = new Electricity();
        }
        return instance;
    }

    public void turnOn(){
        on = true;
        synchronized (this) {
            this.notifyAll();
        }
    }

    public void turnOff(){
        on = false;
    }

    public boolean isOn() {
        return on;
    }
}
