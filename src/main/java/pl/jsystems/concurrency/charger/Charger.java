package pl.jsystems.concurrency.charger;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Charger {

    private static final int POWER = 3;

    public static ExecutorService es = Executors.newScheduledThreadPool(5);

    public void chargeDevice(Phone p, int hours) {
        es.execute( () ->  p.charge(hours*POWER));
    }
}
