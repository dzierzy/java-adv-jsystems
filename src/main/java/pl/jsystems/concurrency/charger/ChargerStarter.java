package pl.jsystems.concurrency.charger;

public class ChargerStarter {

    public static void main(String[] args) {

        Electricity.getInstance().turnOn();

        Charger samsungCharger = new Charger();
        Charger appleCharger = new Charger();

        Phone samsung = new Phone(50, "Samsung S11");
        Phone apple = new Phone(35, "Iphone XR");

        samsungCharger.chargeDevice(samsung, 8);
        appleCharger.chargeDevice(apple, 10);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //Electricity.getInstance().turnOn();

        System.out.println("done.");

        Charger.es.shutdown();

    }
}
