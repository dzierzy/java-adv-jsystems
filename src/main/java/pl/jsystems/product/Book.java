package pl.jsystems.product;

import java.util.Objects;

public class Book extends Product{

    @Autoconfigurable(defaultValue = "Unknown")
    private String title;

    public Book(String title, int price) {
        this.title = title;
        this.setPrice(price);
    }

    public Book(){
    }

    public Book(int i){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title) && Objects.equals(price, book.price) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, price);
    }
}
