package pl.jsystems.product;

import java.io.Serializable;


public abstract class Product /*extends Object*/ implements Serializable {

    @Autoconfigurable(defaultValue = "0")
    protected int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                '}';
    }
}
