package pl.jsystems.reflect;



import pl.jsystems.product.Product;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class ReflectStarter {

    public static void main(String[] args) throws ClassNotFoundException, IOException {

        /*InputStream is = ReflectStarter.class.getResourceAsStream("/classname.properties");

        Properties props = new Properties();
        props.load(is);

        String className = props.getProperty("classname");*/
        ResourceBundle bundle =
                ResourceBundle.getBundle(
                        "classname");

        String className = bundle.getString("classname");
        int price = Integer.parseInt(bundle.getString("price"));

        Class clazz = Class.forName(className);
                //Book.class;
        Class originalClazz = clazz;

        while(clazz!=null) {
            printClassInfo(clazz);
            clazz = clazz.getSuperclass();
            System.out.println("***************************");
        }

        try {
            Constructor constructor = originalClazz.getConstructor();
            Object o = constructor.newInstance();

            if(!(o instanceof Product)){
                throw new IllegalArgumentException("provided class name not instance of Product");
            }
            Product p = (Product) o;

            Field f = getField(originalClazz, "price");
            boolean accessible = f.isAccessible();
            f.setAccessible(true);
            f.set(p, price);
            f.setAccessible(accessible);

            System.out.println("product name:" + p.getClass().getName() + ", price:" + p.getPrice());

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static Field getField(Class clazz, String fieldName) {
        do {
            try {
                return clazz.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            }
        }while(clazz!=null);
        return null;
    }



    private static void printClassInfo(Class clazz) {
        String name = clazz.getName();
        String simpleName = clazz.getSimpleName();
        Package p = clazz.getPackage();
        String packageName = p.getName();

        System.out.println("name=" + name + ", simpleName=" + simpleName + ", packageName=" + packageName);

        Class[] interfaces = clazz.getInterfaces();
        for(Class interfaceClass : interfaces){
            printClassInfo(interfaceClass);
        }

        Constructor[] constructors = clazz.getConstructors();
        for(Constructor constructor : constructors){
            Class[] parameterTypes = constructor.getParameterTypes();
            System.out.println(
                    "constructor parameters:" + Arrays.asList(parameterTypes).toString());
        }

        Method[] methods = clazz.getDeclaredMethods();
        for(Method m : methods){
            String methodName = m.getName();
            Class[] parameterTypes = m.getParameterTypes();
            Class returnClass = m.getReturnType();
            Class[] exceptionTypes = m.getExceptionTypes();

            int modifiers = m.getModifiers();
            boolean publicMethod = Modifier.isPublic(modifiers);

            System.out.println(
                    publicMethod + " "
                    + returnClass.getSimpleName() + " "
                    + methodName + "(" + Arrays.asList(parameterTypes) + ")"
                    + " throws " + Arrays.asList(exceptionTypes)
            );
        }

        Field[] fields = clazz.getDeclaredFields();
        for(Field f : fields){
            String fieldName = f.getName();
            Class type = f.getType();
            int modifier = f.getModifiers();
            System.out.println(
                    Modifier.isPrivate(modifier) + " "
                    + type + " " + fieldName

            );
        }
    }

}
