package pl.jsystems.reflect;

import pl.jsystems.product.Autoconfigurable;
import pl.jsystems.product.Book;
import pl.jsystems.product.Product;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class AnnotationStarter {

    static Map<Class, Function<String, Object>> mappers = new HashMap<>();

    static {
        mappers.put(int.class, s->Integer.parseInt(s));
        mappers.put(double.class, s->Double.parseDouble(s));
    }

    public static void main(String[] args) throws IllegalAccessException {

        Supplier<Product> productSupplier = getProductSupplier();
        Product p = productSupplier.get();

        Class clazz = p.getClass();
        List<Field> fields = getAnnotatedFields(clazz, Autoconfigurable.class);

        Function<Field, String> fieldValueFunction = getFieldValueFunction();
        for(Field f : fields){
            String fieldValue = fieldValueFunction.apply(f);
            f.setAccessible(true);
            Function<String, Object> mapper = mappers.get(f.getType());
            if(mapper!=null) {
                f.set(p, mapper.apply(fieldValue));
            } else {
                f.set(p, fieldValue);
            }
        }
        Consumer<Product> productConsumer = getProductConsumer();
        productConsumer.accept(p);

    }

    public static Consumer<Product> getProductConsumer(){
        return p -> System.out.println(p);
    }


    public static Function<Field, String> getFieldValueFunction(){
        ResourceBundle bundle = ResourceBundle.getBundle("classname");
        return f -> {
            String fieldName = f.getName();
            String fieldValue = null;
            try {
                fieldValue = bundle.getString(fieldName);
            }catch(MissingResourceException e){
                Autoconfigurable a = f.getAnnotation(Autoconfigurable.class);
                fieldValue = a.defaultValue();
            }
            return fieldValue;
        };
    }

    public static Supplier<Product> getProductSupplier(){
        return Book::new;
                //() -> new Book();

    }

    public static List<Field> getAnnotatedFields(Class clazz, Class annotation){
        List<Field> fieldList = new ArrayList<>();
        do {
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                if (f.isAnnotationPresent(annotation)) {
                    fieldList.add(f);
                }
            }
            clazz = clazz.getSuperclass();
        }while(clazz!=null);
        return fieldList;
    }
}
