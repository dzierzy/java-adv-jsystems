package pl.jsystems.xml;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class SAXProductListProviderTest {

    @Test
    public void testProductCount(){
        ProductListProvider provider = new SAXProductListProvider("/order.xml");

        List<ProductDescription> description = provider.getProductDescriptions();
        Assert.assertNotNull(description);
        Assert.assertEquals(3, description.size());
    }

}