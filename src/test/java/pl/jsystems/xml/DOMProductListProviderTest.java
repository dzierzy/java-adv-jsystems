package pl.jsystems.xml;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DOMProductListProviderTest {


    @Test
    public void testDocumentLoaded(){
        try {
            ProductListProvider provider = new DOMProductListProvider("/order.xml");
        }catch (Exception e){
            e.printStackTrace();
            Assert.fail("exception: " + e.getMessage());
        }
    }

    @Test
    public void testProductCount(){
        ProductListProvider provider = new DOMProductListProvider("/order.xml");
        List<ProductDescription> descriptions = provider.getProductDescriptions();
        Assert.assertNotNull(descriptions);
        Assert.assertEquals(3, descriptions.size());
    }

}